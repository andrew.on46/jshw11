const buttonsWrapper = document.querySelector(".btn-wrapper");
const buttonsList = buttonsWrapper.querySelectorAll(".btn");
let prevTarget = null;

const btnToBlue = (btn) => {
  btn.style.cssText = "background-color: blue;";
};

const btnsToBlack = () => {
  buttonsList.forEach(
    (btn) => (btn.style.cssText = "background-color: black;")
  );
};

window.addEventListener("keydown", (e) => {
  buttonsList.forEach((btn) => {
    if (e.code.replace("Key", "") === btn.innerHTML) {
      if (prevTarget) {
        btnsToBlack(prevTarget);
      }
      prevTarget = e.code;
      btnToBlue(btn);
    }
  });
});
